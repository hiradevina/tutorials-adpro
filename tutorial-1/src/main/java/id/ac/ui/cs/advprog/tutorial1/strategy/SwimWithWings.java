package id.ac.ui.cs.advprog.tutorial1.strategy;

public class SwimWithWings implements SwimBehavior {
    @Override
    public void swim() {
        System.out.println("this duck swims with wings");
    }
}