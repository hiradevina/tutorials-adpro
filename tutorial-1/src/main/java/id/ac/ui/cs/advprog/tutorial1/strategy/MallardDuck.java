package id.ac.ui.cs.advprog.tutorial1.strategy;

public class MallardDuck extends Duck {
    // TODO Complete me!
    MallardDuck() {
        setFlyBehavior(new FlyWithWings());
        setQuackBehavior(new MuteQuack());
        setSwimBehavior(new CannotSwim());
    }

    @Override
    public void display() {
        System.out.println("I\'m Mallard Duck");
    }
}
