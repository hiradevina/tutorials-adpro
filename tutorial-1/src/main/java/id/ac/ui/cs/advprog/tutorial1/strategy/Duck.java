package id.ac.ui.cs.advprog.tutorial1.strategy;

public abstract class Duck {

    private FlyBehavior flyBehavior;
    private QuackBehavior quackBehavior;
    private SwimBehavior swimBehavior;

    public void performFly() {
        flyBehavior.fly();
    }

    public void performQuack() {
        quackBehavior.quack();
    }
    
    public void swim() {
        swimBehavior.swim();
    }
    
    public void setFlyBehavior(FlyBehavior f) {
        this.flyBehavior = f;
    }
    public void setQuackBehavior(QuackBehavior q) {
        this.quackBehavior = q;
    }

    public void setSwimBehavior(SwimBehavior s) {
        this.swimBehavior = s;
    }



    public abstract void display();
    // TODO Complete me!
}
