package id.ac.ui.cs.advprog.tutorial1.strategy;

public class CannotSwim implements SwimBehavior {
    @Override
    public void swim() {
        System.out.println("this duck can\'t swim");
    }
}